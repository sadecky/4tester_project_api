import time
from base64 import b64encode
import requests
import pytest
import lorem

username_editor = 'editor'
username_commenter = 'commenter'
password_editor = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
password_commenter = 'SXlx hpon SR7k issV W2in zdTb'
blog_url = 'https://gaworski.net'
posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"
posts_endpoint_comment_url = blog_url + "/wp-json/wp/v2/comments"
token_editor = b64encode(f"{username_editor}:{password_editor}".encode('utf-8')).decode("ascii")
token_commenter = b64encode(f"{username_commenter}:{password_commenter}".encode('utf-8')).decode("ascii")


@pytest.fixture(scope='module')
def article():
    timestamp = int(time.time())
    article = {
        "article_creation_date": timestamp,
        "article_title": "This is new post " + str(timestamp),
        "article_subtitle": lorem.sentence(),
        "article_text": lorem.paragraph()
    }
    return article


@pytest.fixture(scope='module')
def comment():
    comment = {
        "comment_author": 'commenter',
        "comment_email": 'commenter@somesite.com',
        "comment_text": lorem.paragraph()
    }
    return comment


@pytest.fixture(scope='module')
def answer():
    answer = {
        "answer_author": 'editor',
        "answer_email": 'editor@somesite.com',
        "answer_text": lorem.paragraph()
    }
    return answer


@pytest.fixture(scope='module')
def headers_editor():
    headers_editor = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_editor
    }
    return headers_editor


@pytest.fixture(scope='module')
def headers_comment():
    headers_comment = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_commenter
    }
    return headers_comment


@pytest.fixture(scope='module')
def posted_article(article, headers_editor):
    payload = {
        "title": article["article_title"],
        "excerpt": article["article_subtitle"],
        "content": article["article_text"],
        "status": "publish"
    }
    response = requests.post(url=posts_endpoint_url, headers=headers_editor, json=payload)
    return response


@pytest.fixture(scope='module')
def posted_comment(comment, headers_comment, posted_article):
    wordpress_post_id = posted_article.json()["id"]
    comments_endpoint_url = f'{posts_endpoint_comment_url}'
    payload = {
        "post": wordpress_post_id,
        "author_name": comment["comment_author"],
        "author_email": comment["comment_email"],
        "content": comment["comment_text"],
        "status": "publish"
    }
    response = requests.post(url=comments_endpoint_url, headers=headers_comment, json=payload)
    return response


@pytest.fixture(scope='module')
def posted_answer(answer, headers_editor, posted_article, posted_comment):
    wordpress_post_id = posted_article.json()["id"]
    id_parent = posted_comment.json()["id"]
    answer_url = f'{posts_endpoint_comment_url}'
    payload = {
        "parent": id_parent,
        "post": wordpress_post_id,
        "author_name": answer["answer_author"],
        "author_email": answer["answer_email"],
        "content": answer["answer_text"],
        "status": "publish"

    }
    response = requests.post(url=answer_url, headers=headers_editor, json=payload)
    return response


def test_new_post_is_successfully_created(posted_article, headers_editor):
    assert posted_article.status_code == 201
    assert posted_article.reason == "Created"


def test_new_post_comment_is_successfully_created(posted_comment, headers_editor):
    assert posted_comment.status_code == 201
    assert posted_comment.reason == "Created"


def test_new_post_comment_answer_is_successfully_created(posted_answer, headers_editor, headers_comment):
    assert posted_answer.status_code == 201
    assert posted_answer.reason == "Created"


def test_verify_authorship_of_the_article(posted_article, headers_editor):
    wordpress_post_id = posted_article.json()["id"]
    wordpress_post_url = f'{posts_endpoint_url}/{wordpress_post_id}'
    response = requests.get(url=wordpress_post_url, headers=headers_editor)
    assert response.status_code == 200
    assert response.reason == "OK"
    wordpress_post_data = response.json()
    user_editor = 2
    assert wordpress_post_data['author'] == user_editor


def test_verify_authorship_of_the_comment(posted_comment, headers_comment):
    wordpress_post_id = posted_comment.json()["id"]
    wordpress_post_url = f'{posts_endpoint_comment_url}/{wordpress_post_id}'
    response = requests.get(url=wordpress_post_url, headers=headers_comment)
    assert response.status_code == 200
    assert response.reason == "OK"
    wordpress_post_data = response.json()
    assert wordpress_post_data['author_name'] == 'commenter'


def test_verify_authorship_of_the_comment_answer(posted_answer, headers_editor):
    wordpress_post_id = posted_answer.json()["id"]
    wordpress_post_url = f'{posts_endpoint_comment_url}/{wordpress_post_id}'
    response = requests.get(url=wordpress_post_url, headers=headers_editor)
    assert response.status_code == 200
    assert response.reason == "OK"
    wordpress_post_data = response.json()
    assert wordpress_post_data['author_name'] == 'editor'


def test_relationship_between_article_and_comment(posted_article, posted_comment):
    wordpress_post_id = posted_article.json()["id"]
    wordpress_comment_id = posted_comment.json()["id"]

    response = requests.get(f"{posts_endpoint_comment_url}/{wordpress_comment_id}")
    assert response.status_code == 200
    assert response.reason == "OK"
    comment_data = response.json()
    assert comment_data["post"] == wordpress_post_id


def test_relationship_between_comment_and_answer(posted_comment, posted_answer):
    comment_id = posted_comment.json()["id"]
    answer_id = posted_answer.json()["parent"]

    response = requests.get(f"{posts_endpoint_comment_url}/{comment_id}")
    comment_data = response.json()

    assert response.status_code == 200
    assert comment_data.get('id') == answer_id


def test_relationship_between_answer_and_article(posted_answer, posted_article):
    answer_id = posted_answer.json()["id"]
    article_id = posted_article.json()["id"]

    response = requests.get(f"{posts_endpoint_comment_url}/{answer_id}")
    answer_data = response.json()

    assert response.status_code == 200
    assert answer_data.get("post") == article_id